<?php
require_once '../back/DB.php';
$db = new DB();
$seller = $db->auth_user();
if (!$seller) {
    header("Location: ../");
    die();
}
?>

<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Личный кабинет</title>
    <link href="index.css" rel="stylesheet">
</head>
<body>
<?php require_once '../header.php' ?>
<main>
    <h2>Личный кабинет продавца <?= $seller['email'] ?></h2>
    <h3>Создание товара</h3>
    <form action="../back/create_product.php" method="post" enctype="multipart/form-data">
        <input placeholder="Название товара..." name="title">
        <select name="category">
            <option value="0" disabled selected>Выбрать категорию..</option>
            <?
            $categories = $db->query("SELECT * FROM categories")->get_result()->fetch_all(1);
            foreach ($categories as $category) {
                echo "<option value='$category[id]'>$category[title]</option>";
            }
            ?>
        </select>
        <input placeholder="Стоимость при поставке..." name="cost">
        <input placeholder="Цена при реализации..." name="price">
        <input type="file" placeholder="Фото товара" name="photo">
        <button type="submit">Создать товар</button>
    </form>
    <div id="columns">
        <div>
            <h3>Список товаров</h3>
            <?
            $products = $db->query("SELECT * FROM products")->get_result()->fetch_all(1);
            foreach ($products as $product) {
                $income = $db->query("SELECT sum(amount) amount FROM admissions WHERE product = ?", 'i', $product['id'])->get_result()->fetch_assoc()['amount'];
                $outcome = $db->query("SELECT sum(amount) amount FROM ord_prod WHERE prod_id = ?", 'i', $product['id'])->get_result()->fetch_assoc()['amount'];
                $amount = $income - $outcome;
                echo "<form action='../back/edit_product.php'>$product[name]<br/>" .
                    "Количетство: $amount<br/>" .
                    "Оформить поставку: <input name='admission' placeholder='Кол-во...'/>" .
                    "<input type='hidden' name='prod_id' value='$product[id]'/>" .
                    "</form>";

            }
            ?>
        </div>
        <div>
            <h3>Список заказов</h3>
            <?
            $orders = $db->query("SELECT * FROM orders where seller_id = ? OR status = 'Оформлен'",'i', $seller['id'])->get_result()->fetch_all(1);
            foreach ($orders as $order){
                if($order['user_id']){
                    $user_email = $db->query("SELECT email FROM users WHERE id = ?",'i', $order['user_id'])->get_result()->fetch_assoc()['email'];
                    $user_data = "Пользователь: ".$user_email;
                } else {
                    $user_data = "Гость: $order[guest_name] $order[guest]. Телефон: $order[guest_phone]";
                }
                $products = $db->query(
                    "SELECT * FROM ord_prod 
                    LEFT JOIN products on ord_prod.prod_id = products.id
                    WHERE ord_id = ?",'s', $order['id']
                )->get_result()->fetch_all(1);
                echo "<div>" .
                    "   Заказ <b>$order[id]</b><br/>" .
                    "   $user_data <br/>";

                foreach ($products as $product){echo "$product[name]. $product[amount] шт.<br/>";}

                echo "<form action='../back/edit_order.php'>" .
                    "   <input type='hidden' name='order_id' value='$order[id]'/>".
                    "   <select name='status'>" .
                    "       <option value='Оформлен' ".($order['status'] === 'Оформлен' ? "selected" : "").">Оформлен</option>" .
                    "       <option value='Деньги пришли' ".($order['status'] === 'Деньги пришли' ? "selected" : "").">Деньги пришли</option>" .
                    "       <option value='Товар ушел' ".($order['status'] === 'Товар ушел' ? "selected" : "").">Товар ушел</option>" .
                    "   </select>".
                    "   <button type='submit'>Изменить</button>".
                    "</form>";

                echo "</div>";
            }
            ?>
        </div>
    </div>
</main>
</body>
</html>
