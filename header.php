<link href="/header.css" rel="stylesheet">
<div id="header">
    <div id="header-title"><a href="/">Магазин аудио техники</a></div>
    <div class="card_btn"><a href="/cart">Корзина (<?=count(json_decode($_COOKIE['cart'],1) ?? [])?>)</a></div>
    <?php
    require_once "back/DB.php";
    $db = new DB();
    $user = $db->auth_user();
    if(!$user):
    ?>
    <a href="/sign_in">Войти</a>
    <a href="/sign_up">Зарегистрироваться</a>
    <?php else: ?>
    <a href="/<?=$user['group']?>"><?=$user['email']?></a>
    <form action="back/log_out.php">
        <input type="submit" value="Выйти">
    </form>
    <?php endif;?>
</div>
