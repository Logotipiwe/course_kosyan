<?php

class Methods
{
    const without_auth = ['sign_in', 'sign_up', 'order_new'];

    public static function say_hi($data = null, DB $db = null)
    {
        return self::ok();
    }

    public static function ok($ans = null)
    {
        if (!is_null($ans)) return json_encode(["ok" => true, 'ans' => $ans]);
        return json_encode(["ok" => true]);
    }

    public static function err($err, $err_msg = null)
    {
        if ($err_msg) return json_encode(['ok' => false, 'err' => $err, 'err_msg' => $err_msg]);
        return json_encode(['ok' => false, 'err' => $err]);
    }

    private static function set_auth_cookie($user, DB $db)
    {
        $login = $user['email'];
        $token = md5($login . $user['password'] . $user['rand']); //ТОКЕН ВЫЧИСЛЯЕТСЯ КАК ЛОГИН+ПАРОЛЬ+РАНДОМ И ВСЕ ЭТО ХЕШИРУЕТСЯ
        //ПО ЭТОМУ ТОКЕНУ БУДЕТ ОПРЕДЕЛЯТЬСЯ АВТОРИЗАЦИЯ
        //ТАКИМ ОБРАЗОМ ЕСЛИ РАНДОМ ЗНАЧЕНИЕ СМЕНИТСЯ, ТОКЕН ТОЖЕ СМЕНИТСЯ И ПРЕДЫДУЩИЙ ТОКЕН СТАНЕТ НЕ ВАЛИДЕН

        setcookie('login', $login, time() + 3600 * 24 * 7, '/');
        setcookie('token', $token, time() + 3600 * 24 * 7, '/');
        return;
    }

    public static function sign_out($data, DB $db)
    {
        setcookie('login', '', 0, '/');
        setcookie('token', '', 0, '/');

        header("Location: /course");
        die();
    }

    public static function redirect_to_info_page($msg, $is_error = false)
    {
        $err_str = ($is_error) ? "&type=err" : '';
        header("Location: /course/info_page.php?msg=$msg".$err_str);
        die();
    }

    public static function sign_up($data, DB $db)
    {
        $login = $data['login'];
        $name = $data['name'];
        $surname = $data['surname'];
        $phone = $data['phone'];

        if ($login === '' OR $data['password1'] === '' OR $data['password2'] !== $data['password1']) { //ЕСЛИ ЛОГИН ПУСТОЙ ИЛИ ПАРОЛИ НЕ СОВПАДАЮТ
            $err_str = (true) ? "&type=err" : '';
            header("Location: /course/info_page.php?msg=Данные введены неверно, перепроверьте поля" . $err_str);
            die(); //ВЕРНУТЬ С СООБЩЕНИЕМ ОБ ОШИБКЕ
            return; //ЗАКОНЧИТЬ ВЫПОЛНЕНИЕ ФУНКЦИИ
        }
        $password = $data['password1'];

        $login_exists = $db->query('SELECT * FROM users where email = ?', 's', $login)->get_result()->num_rows > 0; //ПРОВЕРИТЬ ЕСТЬ ЛИ ЗАПИСИ В БД С ТАКИМ ЛОГИНОМ
        if ($login_exists) { //ЕСЛИ ЕСТЬ - ВЕРНУТЬ С ОШИБКОЙ
            $err_str1 = (true) ? "&type=err" : '';
            header("Location: /course/info_page.php?msg=Логин $login занят, попробуйте другой" . $err_str1);
            die(); //ВЕРНУТЬ С СООБЩЕНИЕМ ОБ ОШИБКЕ
        }

        $password_md5 = md5($password); //ЗАШИФРОВАТЬ ПАРОЛЬ ДЛЯ НАДЕЖНОГО ХРАНЕНИЯ В БД
        $rand = rand(0, 10000); //СГЕНЕРИРОВАТЬ СЛУЧАЙНОЕ ЗНАЧЕНИЕ (ОНО НУЖНО ДЛЯ ВЫЧИСЛЕНИЯ ТОКЕНА)
        $db->query("INSERT INTO users (email, password, rand, name, surname, phone) VALUES (?,?,?,?,?,?)",
            'ssisss',
            $login, $password_md5, $rand, $name, $surname, $phone); //СОЗДАТЬ НОВОГО ЮЗЕРА В БД

//        $user = $db->query("SELECT *, 'user' `group` FROM users WHERE id = ?",'i', $db->last_insert_id())->get_result()->fetch_assoc();
        $login1 = $user['email'];
        $token = md5($login1 . $user['password'] . $user['rand']); //ТОКЕН ВЫЧИСЛЯЕТСЯ КАК ЛОГИН+ПАРОЛЬ+РАНДОМ И ВСЕ ЭТО ХЕШИРУЕТСЯ
        //ПО ЭТОМУ ТОКЕНУ БУДЕТ ОПРЕДЕЛЯТЬСЯ АВТОРИЗАЦИЯ
        //ТАКИМ ОБРАЗОМ ЕСЛИ РАНДОМ ЗНАЧЕНИЕ СМЕНИТСЯ, ТОКЕН ТОЖЕ СМЕНИТСЯ И ПРЕДЫДУЩИЙ ТОКЕН СТАНЕТ НЕ ВАЛИДЕН

        setcookie('login', $login1, time() + 3600 * 24 * 7, '/');
        setcookie('token', $token, time() + 3600 * 24 * 7, '/');
        //ОТДАТЬ ПОЛЬЗОВАТЕЛЮ КУКИ С ЛОГИНОМ И ТОКЕНОМ ДЛЯ АВТОРИЗАЦИИ

        header("Location: /course");
    }

    public static function sign_in($data, DB $db)
    {
        $login = $data['login'];
        $password = $data['password'];

        $user = $db->query("SELECT * FROM users WHERE email = ? AND password = ?",'ss', $login, md5($password))->get_result();

        if (!$user) { //ЕСЛИ НЕТ ОДНОЙ ЗАПИСИ ПОЛЬЗОВАТЕЛЯ С ТАКИМ ЛОГИНОМ И ПАРОЛЕМ В БД - ВЕРНУТЬ С ОШИБКОЙ
            header("Location: /course/info_page.php?msg=Данные введены неверно, перепроверьте поля");
            die(); //ВЕРНУТЬ С СООБЩЕНИЕМ ОБ ОШИБКЕ
        }

        setcookie('login', $login, time() + 3600 * 24 * 7, '/');
        setcookie('token', md5($user['id'].$user['login'].$user['password']), time() + 3600 * 24 * 7, '/'); //ВЕРНУТЬ КУКИ С ЛОГИНОМ И ТОКЕНОМ

        header("Location: ../user"); //ПЕРЕНАПРАВИТЬ
    }

    public static function order_new($data, DB $db)
    {
        $user = $db->auth_user();
        if($user['group'] !== 'user') self::redirect_to_info_page("Продавец или владелец не могут оформить заказ. Зарегистрируйте аккаунт покупателя");
        $cart = json_decode($data['cart_str'], 1);

        if (is_null($cart)) {
            header("Location: ../info_page.php?msg=Ошибка оформленя заказа&type=err");
            die();
        }
        $order_id = mb_strtoupper($user['id'].base_convert(time(), 10, 32));
        $db->query("INSERT INTO orders (id) VALUES (?)", 's', $order_id);
        foreach ($cart as $prod_id => $prod) {
            $db->query(
                "INSERT INTO ord_prod (ord_id, prod_id, amount, price) VALUES (?,?,?,?)",
                'siii',
                $order_id, $prod_id, $prod['count'], $prod['price']
            );
        }
        if ($user) {
            $user_id = $user['id'];
            ['name' => $name, 'surname' => $surname, 'phone' => $phone] = $user;
            $db->query(
                "INSERT INTO ord_reg (ord_id, user_id) VALUES (?, ?)",
                'si',
                $order_id, $user_id
            );
        } else {
            $name = $data['name'];
            $surname = $data['surname'];
            $phone = $data['phone'];
            $db->query(
                "INSERT INTO ord_guest (ord_id, name, surname, phone) VALUES (?,?,?,?)",
                'ssss',
                $order_id, $name, $surname, $phone
            );
        }

        $msg = urlencode("Заказ оформлен успешно, $name $surname. Номер заказа $order_id. Мы свяжемся с вами по телефону $phone");

        self::redirect_to_info_page($msg);
    }

    public static function user_data_set($data, DB $db)
    {
        $user = $db->auth_user();
        $name = $data['name'];
        $surname = $data['surname'];
        $phone = $data['phone'];
        $db->query("UPDATE users SET name = ?, surname = ?, phone = ? WHERE id = ?",'sssi',
            $name, $surname, $phone, $user['id']);

        header("Location: /course/user/");
        die();
    }

    public static function seller_data_set($data, DB $db)
    {
        $user = $db->auth_user();
        if($user['group'] !== 'owner' AND $user['group'] !== 'seller') return;

        $name = $data['name'];
        $surname = $data['surname'];
        $pat = $data['pat'];
        $passport = $data['passport'];

        $seller_id = (isset($data['seller_id'])) ? $data['seller_id'] : $user['id'];

        $db->query("UPDATE seller SET name = ?, surname = ?, patronymic = ?, passport = ? WHERE id = ?",'ssssi',
            $name, $surname, $pat, $passport, $seller_id);

        header("Location: /course/$user[group]/");
        die();
    }

    public static function user_password_set($data, DB $db)
    {
        $user = $db->auth_user();
        $table = ($user['group'] === 'user') ? 'users' : $user['group'];
        $curr_password = $data['curr_password'];
        $new_password = $data['password'];
        $password2 = $data['password2'];

        /** @noinspection SqlResolve */
        $valid_password_md5 = $db->query(
            "SELECT password FROM ".$table." WHERE id = ?",'i', $user['id']
        )->get_result()->fetch_assoc()['password'];

        if(
            md5($curr_password) !== $valid_password_md5
            OR $new_password !== $password2
            OR $new_password === ''
        ){
            self::redirect_to_info_page("Ошибка обновления пароля", true);
        }

        $db->query("UPDATE ".$table." SET password = ? WHERE id = ?",'si', md5($new_password), $user['id']);
        self::set_auth_cookie($user, $db);
        self::redirect_to_info_page("Пароль обновлён");
    }

    public static function order_confirm($data, DB $db)
    {
        $user = $db->auth_user();
        if($user['group'] !== 'seller') return;

        $order_id = $data['order_id'];
        $db->query("UPDATE orders SET status = 'Подтвержден', seller_id = ? WHERE id = ?",'is',
            $user['id'], $order_id);

        header("Location: /course/seller");
    }

    public static function order_cancel($data, DB $db)
    {
        if($db->auth_user()['group'] !== 'seller') return;

        $order_id = $data['order_id'];
        $db->query("UPDATE orders SET status = 'Отменён' WHERE id = ?",'s', $order_id);

        header("Location: /course/seller");
    }

    public static function set_prod_data($data, DB $db)
    {
        $user = $db->auth_user();
        if($user['group'] !== 'seller') return;

        $price = $data['price'];
        $cost = $data['cost'];
        $prod_id = $data['prod_id'];
        $db->query("UPDATE products SET price = ?, cost = ? WHERE id = ?",'iii',
            $price, $cost, $prod_id);
        header("Location: /course/seller");
    }

    public static function add_admission($data, DB $db)
    {
        $user = $db->auth_user();
        if($user['group'] !== 'seller') return;

        $amount = $data['amount'];
        $prod_id = $data['prod_id'];
        $product = $db->get_product_data($prod_id);
        $db->query("INSERT INTO admissions (product, amount, cost) VALUES (?,?,?)",'iii',
            $prod_id, $amount, $product['cost']);

        header("Location: /course/seller");
    }

    public static function del_product($data, DB $db)
    {
        $user = $db->auth_user();
        if($user['group'] !== 'seller') return;
        $prod_id = $data['prod_id'];
        $db->query("DELETE FROM products WHERE id = ?",'i', $prod_id);

        header("Location: /course/seller");
    }

    public static function new_seller($data, DB $db)
    {
        $user = $db->auth_user();
        if($user['group'] !== 'owner') return;

        $login = $data['login'];
        $password = $data['password'];
        $name = $data['name'];
        $surname = $data['surname'];
        $pat = $data['pat'];
        $passport = $data['passport'];

        $db->query(
            "INSERT INTO seller (email, password, rand, name, surname, patronymic, passport) 
            VALUES (?,?,?,?,?,?,?)",'ssissss',$login, $passport, 1, $name, $surname, $pat, $passport
        );

        header("Location: /course/owner");
    }
}
