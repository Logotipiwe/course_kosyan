<?php
require_once "../back/DB.php";
$db = new DB();

$user = $db->auth_user($_COOKIE['loc_login'], $_COOKIE['password']);

if($user['group'] !== 'seller') {
    header("Location: ../info_page.php?msg=У вас нет прав доступа");
    die();
}

$title = $_POST['title'];
$cost = (int)$_POST['cost'];
$price = (int)$_POST['price'];
$category = (int)$_POST['category'];
$photo = $_FILES['photo'];

if($title === "" OR $cost === 0 OR $price === 0 OR $category === 0 OR $photo['type'] !== 'image/png'){
    header("Location: ../info_page.php?msg=Неверно введены данные товара!");
    die();
}

$db->query("INSERT INTO products (category, name, cost, price) VALUES (?,?,?,?)", 'isii', $category, $title, $cost, $price);
$prod_id = $db->inserted_id();
move_uploaded_file($photo['tmp_name'], "../img/$prod_id.png");

header("Location: /seller/");

