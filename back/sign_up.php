<?php
require_once 'DB.php';

$db = new DB();

$login = $_POST['login'];
$name = $_POST['name'];
$surname = $_POST['surname'];
$phone = $_POST['phone'];
$password = $_POST['password1'];
$password2 = $_POST['password2'];

if ($login === '' OR $password === '' OR $password2 !== $password OR $name === '') { //ЕСЛИ ЛОГИН ПУСТОЙ ИЛИ ПАРОЛИ НЕ СОВПАДАЮТ
    header("Location: /info_page.php?msg=Данные введены неверно, перепроверьте поля");
    die(); //ЗАКОНЧИТЬ ВЫПОЛНЕНИЕ ФУНКЦИИ
}

//ПРОВЕРИТЬ ЕСТЬ ЛИ ЗАПИСИ В БД С ТАКИМ ЛОГИНОМ
$users_with_same_login = $db->query("SELECT * FROM users where email = ?", 's', $login)->get_result()->num_rows;

if ($users_with_same_login > 0) { //ЕСЛИ ЕСТЬ - ВЕРНУТЬ С ОШИБКОЙ
    header("Location: /info_page.php?msg=Логин $login занят, попробуйте другой");
    die(); //ВЕРНУТЬ С СООБЩЕНИЕМ ОБ ОШИБКЕ
}

$db->query("INSERT INTO users (email, password, name, surname, phone) VALUES (?,?,?,?,?)",
    'sssss',
    $login, md5($password), $name, $surname, $phone); //СОЗДАТЬ НОВОГО ЮЗЕРА В БД

$token = md5($login . md5($password)); //ТОКЕН ВЫЧИСЛЯЕТСЯ КАК ЛОГИН+ПАРОЛЬ И ВСЕ ЭТО ХЕШИРУЕТСЯ

//ОТДАТЬ ПОЛЬЗОВАТЕЛЮ КУКИ С ЛОГИНОМ И ТОКЕНОМ ДЛЯ АВТОРИЗАЦИИ
setcookie('login', $login, time() + 3600 * 24 * 7, '/');
setcookie('token', $token, time() + 3600 * 24 * 7, '/');

header("Location: /");
