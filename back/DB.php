<?php

class DB
{
    private $connection;

    public function __construct()
    {
        $this->connection = new mysqli('sql', 'root', 'root', 'course');
    }

    public function query($query, $types = null, ...$args)
    {
        $statement = $this->connection->prepare($query);

        if (!$statement) echo $this->connection->error;

        if (count($args)) {
            $statement->bind_param($types, ...$args);
        }
        $statement->execute();
        if ($statement->error) echo $statement->error;
        return $statement;
    }

    public function inserted_id()
    {
        return $this->connection->insert_id;
    }













    public function auth_user($login = null, $password = null)
    {
        $is_check_password = (!is_null($login) AND !is_null($password));
        if (!$is_check_password) {
            $login = (isset($_COOKIE['login'])) ? $_COOKIE['login'] : '';
            $token_or_password = (isset($_COOKIE['token'])) ? $_COOKIE['token'] : '';

            $user_res = $this->query("SELECT * FROM users WHERE email = ?", 's', $login)->get_result();
            $seller_res = $this->query("SELECT * FROM seller WHERE email = ?", 's', $login)->get_result();
            $owner_res = $this->query("SELECT * FROM owner WHERE email = ?", 's', $login)->get_result();
        } else {
            $token_or_password = $password;
            $user_res = $this->query("SELECT * FROM users WHERE email = ? AND password = ?", 'ss', $login, md5($token_or_password))->get_result();
            $seller_res = $this->query("SELECT * FROM seller WHERE email = ? AND password = ?", 'ss', $login, md5($token_or_password))->get_result();
            $owner_res = $this->query("SELECT * FROM owner WHERE email = ? AND password = ?", 'ss', $login, md5($token_or_password))->get_result();
        }

        if ($user_res->num_rows === 1) {
            $auth = $user_res->fetch_assoc();
            $auth['group'] = 'user';
        } else if ($seller_res->num_rows === 1) {
            $auth = $seller_res->fetch_assoc();
            $auth['group'] = 'seller';
        } else if ($owner_res->num_rows === 1) {
            $auth = $owner_res->fetch_assoc();
            $auth['group'] = 'owner';
        } else {
            return false;
        }

        if (!$is_check_password) {
            $valid_token = md5($auth['email'] . $auth['password']);
            if ($token_or_password !== $valid_token) return false;
        }

        return $auth;
    }

    public function get_product_data($product_id)
    {
        return $this->query(
            "SELECT 
                products.*,
                CAST(coalesce(sum(distinct admissions.amount), 0) as signed ) income,
                CAST(coalesce(sum(distinct ord_prod.amount),0) as signed ) outcome 
            FROM products
            LEFT JOIN admissions on products.id = admissions.product
            LEFT JOIN ord_prod on products.id = ord_prod.prod_id
            WHERE products.id = ? 
            GROUP BY products.id", 'i', $product_id
        )->get_result()->fetch_assoc();
    }
}
