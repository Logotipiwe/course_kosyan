<?php
require_once '../back/DB.php';

$admission = (int)$_GET['admission'];
$prod_id = (int)$_GET['prod_id'];

if($admission > 0){
    $db = new DB();
    $cost = $db->query("SELECT cost FROM products WHERE id = ?",'i', $prod_id)->get_result()->fetch_assoc()['cost'];
    $db->query("INSERT INTO admissions (product, amount, cost) VALUES (?,?,?)",'iii', $prod_id, $admission, $cost);
}

header("Location: ../seller/");
