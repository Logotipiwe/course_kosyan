<?php
require_once 'DB.php';
$db = new DB();

$user = $db->query(
    "SELECT * FROM (SELECT email, password FROM users UNION SELECT email,password FROM seller UNION SELECT email, password FROM owner) as t1 WHERE email = ? AND password = ?",
    'ss',
    $_POST['login'], md5($_POST['password'])
)->get_result()->fetch_assoc();
if ($user) {
    setcookie('login', $_POST['login'], time() + 3600 * 24 * 7, '/');
    setcookie('token', md5($user['email'] . $user['password'] . $user['rand']), time() + 3600 * 24 * 7, '/'); //ВЕРНУТЬ КУКИ С ЛОГИНОМ И ТОКЕНОМ

    header("Location: ../"); //ПЕРЕНАПРАВИТЬ На ГЛАВНУЮ
} else {
    header("Location: /info_page.php?msg=Данные введены неверно, перепроверьте поля");
}
