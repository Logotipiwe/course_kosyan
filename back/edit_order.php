<?php
require_once "DB.php";
$db = new DB();

$user = $db->auth_user($_COOKIE['loc_login'], $_COOKIE['token']);
if ($user['group'] !== 'seller') {
    header("Location: ../info_page.php?msg=У вас недостаточно прав доступа");
    die();
}
$order_id = $_GET['order_id'];
$new_status = $_GET['status'];

$db->query("UPDATE orders SET status = ? WHERE id = ?", 'ss', $new_status, $order_id);

if ($new_status !== 'Оформлен') {
    $db->query("UPDATE orders SET seller_id = ? WHERE id = ?", 'is', $user['id'], $order_id);
}

header("Location: /seller");
die();
