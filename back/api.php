<?php
require 'Methods.php';
require 'DB.php';

$method = $_GET['method'];

if (is_callable(['Methods', $method])) {
    $db = new DB();

    if (in_array($method, Methods::without_auth)) {
        $ans = call_user_func(['Methods', $method], $_GET, $db);
    } else {
        $user = $db->auth_user();

        if(!$user) {
            header("Location: ../");
            die();
        }

        $ans = call_user_func(['Methods', $method], $_GET, $db);
    }

    $ans = call_user_func(['Methods', $method], $_GET, $db);

    echo $ans;

}
