<?php
$cart = json_decode($_COOKIE['cart'], 1);

if(!$cart) {
    $cart = [];
}

$new_id = $_GET['id'];
$prod_name = $_GET['name'];

if($cart[$new_id]){
    $cart[$new_id]['amount'] = $cart[$new_id]['amount']+1;
} else {
    $cart[$new_id] = [
        'name' => $prod_name,
        'amount' => 1
    ];
}

setcookie('cart', json_encode($cart), 0, '/');

header('Location: /');
