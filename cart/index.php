<?php
require_once "../back/DB.php";
$db = new DB();
?>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Корзина</title>
    <link href="index.css" rel="stylesheet">
    <script src="../libs/jquery.js"></script>
</head>
<body>
<?php require_once "../header.php"; ?>
<div id="main">
    <h1>Корзина</h1>
    <div id="cart_list">
        <?
        $cart = json_decode($_COOKIE['cart'],1);
        if(!$cart) $cart = [];
        foreach ($cart as $id => $cart_item) {
            echo "<div class='cart-item'>" .
                "   <div>$cart_item[name] ($cart_item[amount] шт.)</div>" .
                "</div>";
        }
        ?>
    </div>
    <form id="form" action="">
            <input type="hidden" name="cart_str" value="" id="cart_inp">
        <?php if (!$db->auth_user()): ?>
            <h3>Введите свои данные</h3>
            <input name="name" placeholder="Имя...">
            <input name="surname" placeholder="Фамилия...">
            <input name="phone" placeholder="Телефон...">
        <?php endif ?>
    </form>
    <form action="/back/clear_cart.php"><input type="submit" value="Отчистить корзину"/></form>
    <input type="submit" form="form" value="Оформить заказ" id="sub_btn">
</div>
</body>
</html>
