<?php
require_once '../back/DB.php';
$db = new DB();
$user = $db->auth_user();
if (!$user) {
    header("Location: ../");
    die();
}
?>

<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Личный кабинет</title>
    <link href="index.css" rel="stylesheet">
</head>
<body>
<?php require_once '../header.php' ?>
<div>
    <h2>Личный кабинет пользователя <?= $user['login'] ?></h2>
</div>
</body>
</html>
