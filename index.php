<?php
require_once "back/DB.php";
$db = new DB();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Магазин аудио техники</title>
    <script src="libs/jquery.js"></script>
    <link href="index.css" rel="stylesheet">
    <link href="header.css" rel="stylesheet">
</head>
<body>
<?php require_once "header.php"; ?>
<div id="main">
    <div id="search">
        <input placeholder="Поиск..."/>
    </div>
    <div id="prods">
        <div id="prods-header">
            <h2>Наши товары </h2>
        </div>
        <div id="prods-list">
            <?php
            $res = $db->query("SELECT * FROM products")->get_result();
            while ($product = $res->fetch_assoc()) {
                $product = $db->get_product_data($product['id']);
                if ($product['income'] - $product['outcome'] < 3) continue;
                echo "
                        <div class='prod-item'>
                            <div class='prod-item-img'><img src='img/prods/$product[id].png'></div>
                            <div class='prod-item-price'>$product[price]</div>
                            <div class='prod-item-title'>$product[name]</div>
                            <form action='back/add_cart.php'>
                                <input type='hidden' name='id' value='$product[id]'>
                                <input type='hidden' name='name' value='$product[name]'>
                                <input type='submit' class='prod-item-add_cart' value='В корзину'/>
                            </form>
                        </div>                        
                        ";
            }
            ?>
        </div>
    </div>
</div>
</body>
</html>
